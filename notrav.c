#include<stdio.h>
#include<stdlib.h>
int ans=1;
typedef struct node
{
    int val ;
    struct node* next;
} node ;

node* push(int x,node* head)
{
    node* temp = (node*)malloc(sizeof(node)) ;
    temp->next = head ;
    temp->val = x ;
    return temp ;
}

node* pop(node* head)
{
    if(head == NULL) return head ;
    node* temp = head ;
    head = head->next ;
    free(temp) ;
    return head ;
}

int top(node* head)
{
    if(head == NULL) return -1 ;
    return head->val ;
}

void add_edge(int x , int y , node* head[])
{
	head[x] = push(y,head[x]);
	head[y] = push(x,head[y]);
	return;
}

void disp(node *head)
{
	while(head!=NULL)
	{
		printf("%d ",head->val);
		head=head->next;	
	}
	
	printf("\n");
	return;
}

int dfs(int n , int p , node* head[])
{
	int count=0;
	node *temp=head[n];
	while(temp!=NULL)
	{
		if(temp->val==p)
		{
			temp=temp->next;
			continue;
		}
		else
		{
			count++;
			dfs(temp->val,n,head);
			if(count!=0)
				ans*=count;
		}
		temp=temp->next;
	}

	return count+1;
}

int main()
{
	int n,i,z;
	scanf("%d",&n);
	node* head[10000];
	for(i=0 ; i<n+1 ;++i)
		head[i]=NULL;

	z=n;
	n=n-1;
	while(n--)
	{ 
		int x,y;
		scanf("%d %d",&x,&y);
		add_edge(x,y,head);
	}
	//printf("22");
	//for(i=1 ; i<z+1 ; i++)
	//	disp(head[i]);
	scanf("%d",&n);
	dfs(n,-5,head);
	printf("%d->%d\n",n,ans);
    return 0 ;
}
