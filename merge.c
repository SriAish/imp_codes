#include<stdio.h>

void merge(int arr[],int in,int mid,int end)
{
	int s1=mid-in+1;
	int s2=end-mid;

	int a1[s1], a2[s2];
	int i,j=0,k=0;
	for(i=0;i<s1;i++)
		a1[i]=arr[in+i];

	for(i=0;i<s2;i++)
		a2[i]=arr[mid+1+i];
	i=in;
	while(j<s1 && k<s2)
	{
		if(a1[j]<a2[k])
		{
			arr[i]=a1[j];
			j++;
		}
		else
		{
			arr[i]=a2[k];
			k++;
		}
		i++;
	}

	if(j==s1)
	{
		for(;k<s2;k++)
		{
			arr[i]=a2[k];
			i++;
		}
	}

	if(k==s2)
	{
		for(;j<s1;j++)
		{
			arr[i]=a1[j];
			i++;
		}
	}

	return;
}

void sort(int arr[] , int in , int end)
{
	if(in>=end)
		return;

	int mid=in+(end-in)/2;
	sort(arr,in,mid);
	sort(arr,mid+1,end);

	merge(arr,in,mid,end);

	return;
}
int main()
{
	int n , i , arr[100];

	scanf("%d",&n);

	for(i=0;i<n;i++)
		scanf("%d",&arr[i]);

	sort(arr,0,n-1);

	for(i=0;i<n;i++)
		printf("%d ",arr[i]);

	printf("\n");
	return 0;
}
