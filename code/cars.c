#include<stdio.h>
long long int arr[2][100005];
void merge1(long long int in,long long int mid,long long int end)
{
	long long int s1=mid-in+1;
	long long int s2=end-mid;

	long long int a1[2][s1], a2[2][s2];
	long long int i,j=0,k=0;
	for(i=0;i<s1;i++)
	{
		a1[0][i]=arr[0][in+i];
		a1[1][i]=arr[1][in+i];
	}

	for(i=0;i<s2;i++)
	{
		a2[0][i]=arr[0][mid+1+i];
		a2[1][i]=arr[1][mid+1+i];
	}

	i=in;
	while(j<s1 && k<s2)
	{
		if(a1[0][j]<a2[0][k])
		{
			arr[0][i]=a1[0][j];
			arr[1][i]=a1[1][j];
			j++;
		}
		else if(a1[0][j]==a2[0][k])
		{
			if(a1[1][j]<a2[1][k])
			{
				arr[0][i]=a1[0][j];
				arr[1][i]=a1[1][j];
				j++;
			}
			else
			{
				arr[0][i]=a2[0][k];
				arr[1][i]=a2[1][k];
				k++;
			}
		}
		else
		{
			arr[0][i]=a2[0][k];
			arr[1][i]=a2[1][k];
			k++;
		}
		i++;
	}
	if(j==s1)
	{
		for(;k<s2;k++)
		{
			arr[0][i]=a2[0][k];
			arr[1][i]=a2[1][k];
			i++;
		}
	}
	if(k==s2)
	{
		for(;j<s1;j++)
		{
			arr[0][i]=a1[0][j];
			arr[1][i]=a1[1][j];
			i++;
		}
	}
	return;
}

void sort1(long long int in ,long long int end)
{
	if(in>=end)
		return;

	long long int mid=in+(end-in)/2;
	sort1(in,mid);
	sort1(mid+1,end);
	merge1(in,mid,end);

	return;
}
long long int merge(long long int arr[],long long int in,long long int mid,long long int end)
{
	long long int inv=0;
	long long int s1=mid-in+1;
	long long int s2=end-mid;

	long long int a1[s1], a2[s2];
	long long int i,j=0,k=0;
	for(i=0;i<s1;i++)
		a1[i]=arr[in+i];

	for(i=0;i<s2;i++)
		a2[i]=arr[mid+1+i];
	i=in;
	while(j<s1 && k<s2)
	{
		if(a1[j]<=a2[k])
		{
			arr[i]=a1[j];
			j++;
		}
		else
		{
			arr[i]=a2[k];
			inv+=(s1-j);
			k++;
		}
		i++;
	}
	if(j==s1)
	{
		for(;k<s2;k++)
		{
			arr[i]=a2[k];
			i++;
		}
	}
	if(k==s2)
	{
		for(;j<s1;j++)
		{
			arr[i]=a1[j];
			i++;
		}
	}
	return inv;
}

long long int sort(long long int arr[] , long long int in , long long int end)
{
	if(in>=end)
		return 0;
	long long int inv=0;
	long long int mid=in+(end-in)/2;
	inv+=sort(arr,in,mid);
	inv+=sort(arr,mid+1,end);
	inv+=merge(arr,in,mid,end);

	return inv;
}
int main()
{
	long long int n , k, i ,brr[100005];

	scanf("%lld %lld",&n,&k);

	for(i=0;i<n;i++)
		scanf("%lld",&arr[0][i]);

	for(i=0;i<n;i++)
		scanf("%lld",&arr[1][i]);

	sort1(0,n-1);


	/*for(i=0;i<n;i++)
		printf("%lld ",arr[0][i]);
	printf("\n");
	for(i=0;i<n;i++)
		printf("%lld ",arr[1][i]);
	printf("\n");*/
	long long int l=0,r=1000000002,mid,ans=-1;

	while(l<=r)
	{
		mid=l+(r-l)/2;

		for(i=0;i<n;i++)
			brr[i]=arr[0][i]+(mid*arr[1][i]);
		
		if(sort(brr,0,n-1)>=k)
		{
			ans=mid;
			r=mid-1;
		}	
		else
			l=mid+1;
	}

	printf("%lld\n",ans);
	return 0;
}
