#include <stdio.h>

long long int n,k;

long long int ma(long long int arr[])
{
	long long int i,j,flag=0,ans=1;
	long long int max[100005],left[100005],right[100005];
	
	for(i=0;i<n;i++)
	{
		if(i%k==0)
			flag=arr[i];

		if(arr[i]>flag)
			flag=arr[i];

		left[i]=flag;
	}
	// for(i=0;i<n;i++)
	// {
	// 	printf("left[%lld]=%lld\n",i,left[i]);
	// }
	flag=arr[n-1];
	for(i=n-1;i>-1;i--)
	{
		if((i)%k==0)
		{
			// printf("i==%lld\n",i);
			flag=arr[i];
		}

		if(arr[i]>flag)
			flag=arr[i];

		right[i]=flag;
	}
	// for(i=0;i<n;i++)
	// {
	// 	printf("right[%lld]=%lld\n",i,right[i]);
	// }
	for (i=k-1,j=0; i < n; ++i,++j)
	{
		// printf("left=%lld right=%lld\n",left[i],right[j]);
		if(left[i]>right[j])
			max[j]=left[i];
		else
			max[j]=right[j];
	}

	for(i=0;i<n-k+1;++i)
	{
		//printf("max[i]=%lld ans=%lld\n",max[i],ans);
		ans*=max[i];
		ans=ans%1000000007;
	}
	
	return ans;
}
long long int  mi(long long int arr[])
{
	long long int i,j,flag=0,ans=1;
	long long int min[100005],left[100005],right[100005];
	
	for(i=0;i<n;i++)
	{
		if(i%k==0)
			flag=arr[i];

		if(arr[i]<flag)
			flag=arr[i];

		left[i]=flag;
	}
	flag=arr[n-1];
	for(i=n-1;i>-1;i--)
	{
		if((i)%k==0)
			flag=arr[i];

		if(arr[i]<flag)
			flag=arr[i];

		right[i]=flag;
	}
	
	for (i=k-1,j=0; i < n; ++i,++j)
	{
		if(left[i]<right[j])
			min[j]=left[i];
		else
			min[j]=right[j];
	}

	for(i=0;i<n-k+1;++i)
	{
		// printf("min[i]=%lld ans=%lld\n",min[i],ans);
		ans*=min[i];
		ans=ans%1000000007;
	}
	
	return ans;
}
long long int expo(long long int n)
{
	long long int ans=1,mul=n,k=1000000005;

	while(k!=0)
	{
		if(k%2==1)
			ans*=mul;

		mul*=mul;
		mul=mul%1000000007;
		ans=ans%1000000007;
		k/=2;
	}
	return ans; 
}

int main()
{
	scanf("%lld %lld",&n,&k);

	long long int arr[100005],i;

	for(i=0;i<n;i++)
		scanf("%lld",&arr[i]);

	long long int max=ma(arr) , min=mi(arr);

	min = expo(min)%1000000007;

	long long int ans = (max*min)%1000000007;

	printf("%lld\n",ans);

	return 0;
}